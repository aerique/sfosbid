#!/bin/sh

env DOCKER_BUILDKIT=1 BUILDKIT_COLORS='run=green:error=red:warning=yellow:cancel=cyan' docker build --tag sfosbid --file Dockerfile .
