#!/bin/sh

docker cp sfosbid-project:/home/nemo/.ssh/id_ed25519 .

# See README.md for explanation.
git ls-files | entr sh -c "rsync -e 'ssh -p 54722 -i id_ed25519' --archive --progress . nemo@127.0.0.1:sfosbid-project/ ; docker exec -it sfosbid-project mb2 -t SailfishOS-latest-aarch64 build ; rsync -e 'ssh -p 54722 -i id_ed25519' nemo@127.0.0.1:sfosbid-project/RPMS/\*.rpm ."

rm -f id_ed25519
