FROM scratch

ARG BUILD_USER=nemo

ADD downloads/Jolla-latest-SailfishOS_Platform_SDK_Chroot-i486.tar.bz2 /

RUN zypper --non-interactive refresh
RUN zypper --non-interactive install openssh-server

RUN groupadd $BUILD_USER
RUN useradd --gid sailfish-system $BUILD_USER
RUN echo "$BUILD_USER ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

COPY downloads/Sailfish_OS-latest-Sailfish_SDK_Tooling-i486.tar.7z /
COPY downloads/Sailfish_OS-latest-Sailfish_SDK_Target-i486.tar.7z /
COPY downloads/Sailfish_OS-latest-Sailfish_SDK_Target-armv7hl.tar.7z /
COPY downloads/Sailfish_OS-latest-Sailfish_SDK_Target-aarch64.tar.7z /

RUN su $BUILD_USER -c "sdk-assistant -y create SailfishOS-latest Sailfish_OS-latest-Sailfish_SDK_Tooling-i486.tar.7z"
RUN su $BUILD_USER -c "sdk-assistant -y create SailfishOS-latest-i486 Sailfish_OS-latest-Sailfish_SDK_Target-i486.tar.7z"
RUN su $BUILD_USER -c "sdk-assistant -y create SailfishOS-latest-armv7hl Sailfish_OS-latest-Sailfish_SDK_Target-armv7hl.tar.7z"
RUN su $BUILD_USER -c "sdk-assistant -y create SailfishOS-latest-aarch64 Sailfish_OS-latest-Sailfish_SDK_Target-aarch64.tar.7z"

RUN rm /Sailfish_OS-latest-Sailfish_SDK_Tooling-i486.tar.7z
RUN rm /Sailfish_OS-latest-Sailfish_SDK_Target-i486.tar.7z
RUN rm /Sailfish_OS-latest-Sailfish_SDK_Target-armv7hl.tar.7z
RUN rm /Sailfish_OS-latest-Sailfish_SDK_Target-aarch64.tar.7z

USER $BUILD_USER
ENV USER=$BUILD_USER
WORKDIR /home/nemo

CMD bash
