# Sailfish OS Builds in Docker

- **status**: maintained for personal use

The canonical home page of this project is
https://codeberg.org/aerique/sfosbid

(The library is also pushed to GitLab and Sourcehut but those sites are not
monitored for support.)

## Dependencies

- Docker
- Git
- Rsync
- SSH
- [entr](https://eradman.com/entrproject/)

## Building the Base Docker Image

- `bin/download.sh`
- `bin/build.sh`

This needs to be redone whenever the SFOSBID project is updated or if you've
pruned your Docker images.

## To Do

- [ ] further automate workflow
- [ ] make getting specific versions of rootfs, tooling and targets easier

## Introduction

So here's yet another build environment for Sailfish OS in Docker. Why?

Basically the others were either too dangerous or too complicated for me or
I just could not use them.

Here's hyperlinks to the alternatives that I know off and might be better
suited for you:

- https://github.com/SfietKonstantin/docker-sailfishos-sdk
- https://github.com/vranki/sailfishdockersdk
- https://github.com/evilJazz/sailfishos-buildengine
- https://github.com/CODeRUS/docker-sailfishos-buildengine
- https://github.com/sailfishos-open/docker-sailfishos-builder

"Sailfish OS Builds in Docker" is inspired on SfietKonstantin's work. I
initially used his version but after I was not careful and blew my `/dev`
away I decided to roll my own after looking at the source of the
alternatives. I felt the `Dockerfile` and helper scripts could be much
simpler for my purposes.

vranki's version came the closest to what I was looking for but I only found
it after I was almost finished.

evilJazz's and CODeRUS's implementations look the most mature but they
require one to install the SDK and to do a lot of handywork. They will
probably work better for more serious and bigger projects but I cannot
install the SDK because it depends on a very old version of OpenSSL which
I'm not prepared to install (and then forget to remove again). Also, for my
Linux distro (Void Linux) it is not just a matter of running the package
manager since it only comes with LibreSSL.

Rinugus' project is relatively recent and looks solid. I couldn't really try
it out without installing QEMU (which is a bit of a pain in the ass in Guix
and I did not want to reboot) and it also wanted to be run using `sudo` and
that is a "no" for me.

Some advantages my project might have:

- does not depend on QEMU
- does not depend on Docker volumes and bind mounts
- does not depend on root access to your filesystem
- does not require installing the Sailfish SDK
- does not require `sudo`

## Testing

These steps assume the C++ Sample repository is cloned in the same directory
as SFOSBID is *and* that you've already build the base SFOSBID image (see
[Building the Base Docker Image](#building-the-base-docker-image)).

- `git clone https://github.com/sailfishos/sample-app-cppqml.git`
- `cd sample-app-cppqml`
- `cp ../sfosbid/Dockerfile-sfosbid-project .`
- `../sfosbid-git/bin/project-build.sh`
- `../sfosbid-git/bin/project-run.sh`
- open another terminal while keeping the one running `project-run.sh`
  running
- `../sfosbid-git/bin/project-sync.sh`
    - you will run into familiar SSH issues such as the server fingerprint
      not being known or the server already having an entry in the
      `known_hosts` file when making a new container, look these up if
      they're unknown to you

You should first see feedback from `rsync`, then the project being built and
finally a package error and warning both of which are not fatal (it's just a
really old project that does not observe current Sailfish dev practices).

If everything went right as written above there should be a
`cppqml-1.0.1.aarch64.rpm` in your directory now. (The version might be
different if it the repo has been updated.)

This RPM can be copied to your phone and installed:

- `scp cppqml-1.0-1.aarch64.rpm defaultuser@192.168.2.15:`
    - use whatever username and IP your phone is reachable on
- `ssh defaultuser@192.168.2.15`
- `devel-su pkcon install-local cppqml-1.0-1.aarch64.rpm`
    - on your phone obviously

You should now have `cppqml` in your app grid. It can be deleted through the
phone UI (same as how you remove other apps) or with `pkcon`.

## Workflow

For the following commands it is assumed you're in your project's main
directory, which is whichever directory you'd normally start a build or run
`qmake` from.

- `cp /path/to/sfosbid/Dockerfile-sfosbid-project .`
- `/path/to/sfosbid/bin/project-build.sh`
- `/path/to/sfosbid/bin/project-run.sh`

The following script runs some suggested commands to automate the rest of
the workflow but it depends on your project and might not work without some
changes. The script is specifically written to work with
https://github.com/sailfishos/sample-app-cppqml

- `/path/to/sfosbid/bin/project-sync.sh`
    1. does a `git ls-files` and has `entr` watching those files for
       changes, so the assumption here is the only files relevant and being
       edited for your project and build are checked into Git
    1. whenever one of those files changes `entr`:
        1. updates the directory inside the Docker container with `rsync`
            - it syncs the whole current directory not just the `git
              ls-files` which is not ideal
        1. runs an aarch64 build
        1. copies the created RPM out of the container (assuming it was put
           in the `RPMS` directory in the container by the build)

To further automate your workflow you can monitor the RPM that is copied out
of the container with `entr` and copy that to your phone whenever it
changes.

And then you could automate for an updated RPM on your phone and
automatically install it. This is all left as an excercise for the reader
because I have not taken these steps yet either.

## Resources

- https://docs.sailfishos.org/Tools/Platform_SDK/SDK_Platform_SDK_Command_Map/

## SSH Public Key Authentication

Please see: https://www.ssh.com/ssh/public-key-authentication/
